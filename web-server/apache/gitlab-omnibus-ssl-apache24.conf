# This configuration has been tested on GitLab 8.2
# Note this config assumes unicorn is listening on default port 8080 and
# gitlab-workhorse is listening on port 8181. To allow gitlab-workhorse to
# listen on port 8181, edit /etc/gitlab/gitlab.rb and change the following:
#
# gitlab_workhorse['listen_network'] = "tcp"
# gitlab_workhorse['listen_addr'] = "127.0.0.1:8181"
#
#Module dependencies
# mod_rewrite
# mod_ssl
# mod_proxy
# mod_proxy_http
# mod_headers

# This section is only needed if you want to redirect http traffic to https.
# You can live without it but clients will have to type in https:// to reach gitlab.
<VirtualHost *:80>
  ServerName YOUR_SERVER_FQDN
  ServerSignature Off

  RewriteEngine on
  RewriteCond %{HTTPS} !=on
  RewriteRule .* https://%{SERVER_NAME}%{REQUEST_URI} [NE,R,L]
</VirtualHost>

<VirtualHost *:443>
  SSLEngine on
  #strong encryption ciphers only
  #see ciphers(1) http://www.openssl.org/docs/apps/ciphers.html
  SSLProtocol all -SSLv2
  SSLHonorCipherOrder on
  SSLCipherSuite "ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS"
  Header add Strict-Transport-Security: "max-age=15768000;includeSubdomains"
  SSLCompression Off
  #CentOS
  SSLCertificateFile /etc/httpd/ssl.crt/YOUR_SERVER_FQDN.crt
  SSLCertificateKeyFile /etc/httpd/ssl.key/YOUR_SERVER_FQDN.key
  SSLCACertificateFile /etc/httpd/ssl.crt/your-ca.crt

  #Debian
  #SSLCertificateFile /etc/ssl/certs/YOUR_SERVER_FQDN.crt
  #SSLCertificateKeyFile /etc/ssl/private/YOUR_SERVER_FQDN.key
  #SSLCACertificateFile /etc/ssl/certs/your-ca.crt
  ServerName YOUR_SERVER_FQDN
  ServerSignature Off

  ProxyPreserveHost On

  # Ensure that encoded slashes are not decoded but left in their encoded state.
  # http://doc.gitlab.com/ce/api/projects.html#get-single-project
  AllowEncodedSlashes NoDecode

  <Location />
    # New authorization commands for apache 2.4 and up
    # http://httpd.apache.org/docs/2.4/upgrading.html#access
    Require all granted

    #Allow forwarding to gitlab-workhorse
    ProxyPassReverse http://127.0.0.1:8181
    ProxyPassReverse http://YOUR_SERVER_FQDN/
  </Location>

# Optional Section for Shibboleth
#  <Location /users/auth/shibboleth/callback>
#    AuthType shibboleth
#    ShibRequestSetting requireSession 1
#    ShibUseHeaders On
#    Require shib-session
#  </Location>
#
#  Alias /shibboleth-sp /usr/share/shibboleth
#
#  <Location /shibboleth-sp>
#    Require all granted
#  </Location>
#
#  <Location /Shibboleth.sso>
#    SetHandler shib
#  </Location>
# end Section for Shibboleth


  # Apache equivalent of nginx try files
  # http://serverfault.com/questions/290784/what-is-apaches-equivalent-of-nginxs-try-files
  # http://stackoverflow.com/questions/10954516/apache2-proxypass-for-rails-app-gitlab
  RewriteEngine on

  # Enable WebSocket reverse Proxy
  # Needs proxy_wstunnel enabled
  RewriteCond %{HTTP:Upgrade} websocket [NC]
  RewriteCond %{HTTP:Connection} upgrade [NC]
  RewriteRule ^/?(.*) "ws://127.0.0.1:8181/$1" [P,L]

# See https://docs.gitlab.com/ee/integration/shibboleth.html#apache-2-4-gitlab-8-6-update
# Don't escape encoded characters in API requests. This makes the IDE work.
# Don't escape encoded characters in -/refs/*/logs_tree/ or -/tree/, since 12.85,
# these are pre-encoded, and excluding them causes double-encoding.
  RewriteCond %{REQUEST_URI} ^/api/v\d+/.* [OR]
  RewriteCond %{REQUEST_URI} .*-/branches/.* [OR]
  RewriteCond %{REQUEST_URI} .*-/refs/.*/logs_tree/.* [OR]
  RewriteCond %{REQUEST_URI} .*-/tree/.*
# Shibboleth
#  RewriteCond %{REQUEST_URI} !/Shibboleth.sso
#  RewriteCond %{REQUEST_URI} !/shibboleth-sp
# end Shibboleth
  RewriteRule .* http://127.0.0.1:8181%{REQUEST_URI} [P,QSA,NE]


  #Forward all requests to gitlab-workhorse except existing files like error documents
  RewriteCond %{DOCUMENT_ROOT}/%{REQUEST_FILENAME} !-f [OR]
  RewriteCond %{REQUEST_URI} ^/uploads/.*
# Shibboleth
#  RewriteCond %{REQUEST_URI} !/Shibboleth.sso
#  RewriteCond %{REQUEST_URI} !/shibboleth-sp
# end Shibboleth
  # Remove "NE" flag to allow special characters (spaces) in path
  RewriteRule .* http://127.0.0.1:8181%{REQUEST_URI} [P,QSA]

  RequestHeader set X_FORWARDED_PROTO 'https'
  RequestHeader set X-Forwarded-Ssl on

  # needed for downloading attachments
  DocumentRoot /opt/gitlab/embedded/service/gitlab-rails/public

  #Set up apache error documents, if back end goes down (i.e. 503 error) then a maintenance/deploy page is thrown up.
  ErrorDocument 404 /404.html
  ErrorDocument 422 /422.html
  ErrorDocument 500 /500.html
  ErrorDocument 502 /502.html
  ErrorDocument 503 /503.html

  # Debian and CentOS distribution defaults provided below
  LogFormat "%{X-Forwarded-For}i %l %u %t \"%r\" %>s %b" common_forwarded

  #For CentOS distributions use
  ErrorLog /var/log/httpd/logs/YOUR_SERVER_FQDN_error.log
  CustomLog /var/log/httpd/logs/YOUR_SERVER_FQDN_forwarded.log common_forwarded
  CustomLog /var/log/httpd/logs/YOUR_SERVER_FQDN_access.log combined env=!dontlog
  CustomLog /var/log/httpd/logs/YOUR_SERVER_FQDN.log combined

  #For Debian distributions use
  #ErrorLog /var/log/apache2/YOUR_SERVER_FQDN_error.log
  #CustomLog /var/log/apache2/YOUR_SERVER_FQDN_forwarded.log common_forwarded
  #CustomLog /var/log/apache2/YOUR_SERVER_FQDN_access.log combined env=!dontlog
  #CustomLog /var/log/apache2/YOUR_SERVER_FQDN.log combined

</VirtualHost>

# GitLab Container Registry Section
# https://gitlab.com/gitlab-org/gitlab-recipes/issues/50

# Prereq: port 4567 needs to be open in firewalls and SELinux.
# See common:tasks/Gitlab.yml for relevant tasks.

Listen 4567
NameVirtualHost *:4567
<VirtualHost *:4567>
  ServerName YOUR_SERVER_FQDN

  ServerSignature Off

  SSLEngine On
  SSLProtocol ALL -SSLv2 -SSLv3
  SSLHonorCipherOrder On
  SSLCipherSuite ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA
  Header add Strict-Transport-Security: "max-age=15768000;includeSubdomains"

  SSLCACertificateFile /etc/pki/tls/certs/ca-bundle.crt

  SSLCertificateFile /etc/pki/tls/certs/YOUR_SERVER_FQDN.cer
  SSLCertificateKeyFile /etc/pki/tls/private/YOUR_SERVER_FQDN.key



  Header set Host "YOUR_SERVER_FQDN:4567"

  RequestHeader set X-Forwarded-Proto "https"

  ProxyRequests Off
  ProxyPreserveHost On
  TimeOut 900

# Timeout didn't resolve issue with Docker container push, so
# added RequestReadTimeout 2019-07-30
  RequestReadTimeout header=20-40,MinRate=500 body=20,MinRate=500

  <Location />
    Order deny,allow
    Allow from all

    ProxyPass http://localhost:5000/ timeout=900
    ProxyPassReverse http://localhost:5000/
  </Location>

  ErrorLog /var/log/httpd/registry-error.log
  LogLevel warn
  CustomLog /var/log/httpd/registry-access.log combined
</VirtualHost>
